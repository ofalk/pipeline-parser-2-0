import yaml
import sys


def load_yaml(file_path):
    # Function to load YAML file and return parsed content
    with open(file_path) as file:
        return yaml.safe_load(file)


def count_stages(parsed_yaml):
    # Function to count the number of stages and output to terminal and artifact file
    stages = parsed_yaml.get("stages", [])
    stage_count = len(stages)

    print(f"\nTotal number of stages: {stage_count}")

    return stage_count


def list_stages(parsed_yaml):
    # Function to list stages defined in the GitLab CI YAML file and provide a count
    stages = parsed_yaml.get("stages", [])
    stage_count = len(stages)

    print("\nStages:")
    for stage in stages:
        print(f"- Stage: {stage}")

    print(f"\nTotal number of stages: {stage_count}")

    return stages


def list_stages_jobs_aggregated(parsed_yaml):
    stages_jobs_aggregated = {"default": []}
    total_jobs = 0
    jobs_with_rules = []  # List of jobs with rules
    jobs_without_rules = []  # List of jobs without rules
    jobs_with_only = []  # List of jobs with "only"
    jobs_with_except = []  # List of jobs with "except"
    jobs_with_needs = []  # List of jobs with "needs"
    jobs_without_needs = []  # List of jobs without "needs"

    # print("# Debug: Print the parsed YAML to check its structureparsed_yaml")
    # print(parsed_yaml)  # Debug: Print the parsed YAML to check its structure

    if "stages" in parsed_yaml:
        for stage in parsed_yaml["stages"]:
            stages_jobs_aggregated[stage] = []

    for key, value in parsed_yaml.items():
        if isinstance(value, dict) and "stage" in value:
            total_jobs += 1
            stage = value.get("stage", "default")
            if stage not in stages_jobs_aggregated:
                stages_jobs_aggregated[stage] = []
            stages_jobs_aggregated[stage].append(key)

            # Initialize checks for "rules", "only", "except", and "needs" for jobs with defined stages
            has_rules = "rules" in value
            has_only = "only" in value
            has_except = "except" in value
            has_needs = "needs" in value

        elif isinstance(value, dict) and key not in ["stages", "variables", "include"]:
            # This is a job without a defined stage
            total_jobs += 1
            stages_jobs_aggregated["default"].append(key)

            # Initialize checks for "rules", "only", "except" and "needs" for jobs without defined stages
            has_rules = "rules" in value
            has_only = "only" in value
            has_except = "except" in value
            has_needs = "needs" in value

        else:
            # Skip non-job items
            continue

        # Update lists based on checks
        if has_rules:
            jobs_with_rules.append(key)
        else:
            jobs_without_rules.append(key)

        if has_only:
            jobs_with_only.append(key)

        if has_except:
            jobs_with_except.append(key)

        if has_needs:
            jobs_with_needs.append(key)
        else:
            jobs_without_needs.append(key)

    return (
        stages_jobs_aggregated,
        jobs_with_rules,
        jobs_without_rules,
        jobs_with_only,
        jobs_with_except,
        jobs_with_needs,
        jobs_without_needs,
    )


def list_jobs_stages(parsed_yaml):
    # Function to list stages each job belongs to and output to terminal and artifact file
    jobs = parsed_yaml.get("jobs", {})

    print("\nJobs and Stages:")
    job_stages = {}
    for job_name, job_config in jobs.items():
        stage = job_config.get("stage", "defaulted to 'test'")
        print(f"- Job1: {job_name}, Stage: {stage}")
        job_stages[job_name] = stage

    # Traverse the YAML structure to find jobs under templates
    for section_name, section_config in parsed_yaml.items():
        if (
            "script" in section_config
            or "extends" in section_config
            or "before_script" in section_config
            or "after_script" in section_config
        ):
            stage = section_config.get("stage", "defaulted to 'test'")
            print(f"- Job2: {section_name}, Stage: {stage}")
            job_stages[section_name] = stage

    return job_stages


def check_gitlab_keywords(parsed_yaml):
    # Function to check which GitLab CI/CD keywords are in use
    gitlab_keywords = [
        "after_script",
        "allow_failure",
        "artifacts",
        "before_script",
        "cache",
        "coverage",
        "dast_configuration",
        "default",
        "dependencies",
        "environment",
        "extends",
        "hooks",
        "id_tokens",
        "identity",
        "image",
        "include",
        "interruptible",
        "inherit",
        "needs",
        "only",
        "pages",
        "parallel",
        "release",
        "resource_group",
        "retry",
        "rules",
        "script",
        "secrets",
        "services",
        "spec",
        "stage",
        "stages",
        "tags",
        "timeout",
        "trigger",
        "variables",
        "when",
    ]

    used_keywords = set()

    # Iterate through parsed YAML content to find used keywords
    for section_name, section_config in parsed_yaml.items():
        if isinstance(section_config, dict):
            for key, value in section_config.items():
                if key in gitlab_keywords:
                    used_keywords.add(key)

    # Sort the used keywords alphabetically
    sorted_used_keywords = sorted(used_keywords)

    # Dictionary to hold documentation links for each keyword
    keyword_docs = {
        "after_script": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#after_script",
        "allow_failure": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#allow_failure",
        "artifacts": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#artifacts",
        "before_script": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#before_script",
        "cache": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#cache",
        "coverage": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#coverage",
        "dast_configuration": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#dast_configuration",
        "dependencies": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#dependencies",
        "default": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#default",
        "environment": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#environment",
        "extends": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#extends",
        "hooks": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#hooks",
        "id_tokens": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#id_tokens",
        "identity": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#identity",
        "image": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#image",
        "include": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#include",
        "inherit": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#inherit",
        "interruptible": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#interruptible",
        "needs": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#needs",
        "only": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#only--except",
        "except": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#only--except",
        "pages": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#pages",
        "parallel": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#parallel",
        "release": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#release",
        "resource_group": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#resource_group",
        "retry": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#retry",
        "rules": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#rules",
        "secrets": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#secrets",
        "services": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#services",
        "spec": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#spec",
        "stage": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#stage",
        "stages": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#stages",
        "tags": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#tags",
        "timeout": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#timeout",
        "trigger": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#trigger",
        "variables": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#variables",
        "when": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#when",
        "workflow": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#workflow",
        "script": "https://docs.gitlab.com/ee/ci/yaml/?query=syntax#script",  # Include 'script' keyword and its documentation link
    }

    return sorted_used_keywords, keyword_docs


def check_gitlab_unused_keywords(parsed_yaml):
    # Function to list unused GitLab CI/CD keywords
    used_keywords, keyword_docs = check_gitlab_keywords(parsed_yaml)
    gitlab_keywords = {
        "after_script",
        "allow_failure",
        "artifacts",
        "before_script",
        "cache",
        "coverage",
        "dast_configuration",
        "default",
        "dependencies",
        "environment",
        "extends",
        "hooks",
        "id_tokens",
        "identity",
        "image",
        "include",
        "interruptible",
        "inherit",
        "needs",
        "only",
        "pages",
        "parallel",
        "release",
        "resource_group",
        "retry",
        "rules",
        "script",
        "secrets",
        "services",
        "spec",
        "stage",
        "stages",
        "tags",
        "timeout",
        "trigger",
        "variables",
        "when",
    }

    unused_keywords = sorted(gitlab_keywords - set(used_keywords))

    # Output unused keywords to HTML file
    with open("unused_keywords.html", "w") as html_file:
        html_file.write('<html style=" ><body>')
        html_file.write("<h1>Unused Keywords in GitLab CI/CD Configuration</h1>")
        if unused_keywords:
            html_file.write("<ul>")
            for keyword in unused_keywords:
                html_file.write(
                    f"<li><a href='{keyword_docs[keyword]}'>{keyword}</a></li>"
                )
            html_file.write("</ul>")
        else:
            html_file.write("<p>No unused keywords found.</p>")
        html_file.write("</body></html>")

    print("Unused Keywords have been written to unused_keywords.html")

    return unused_keywords

    # Output unused keywords to HTML file
    with open("unused_keywords.html", "w") as html_file:
        html_file.write("<html><body>")
        html_file.write("<h1>Unused Keywords in GitLab CI/CD Configuration</h1>")
        if unused_keywords:
            html_file.write("<ul>")
            for keyword in unused_keywords:
                html_file.write(f"<li>{keyword}</li>")
            html_file.write("</ul>")
        else:
            html_file.write("<p>No unused keywords found.</p>")
        html_file.write("</body></html>")

    print("Unused Keywords have been written to unused_keywords.html")

    return unused_keywords

    # Traverse the YAML structure to find jobs under templates
    for section_name, section_config in parsed_yaml.items():
        if "script" in section_config or "extends" in section_config:
            when = section_config.get("when", None)
            if when is not None:
                print(f"- Job3: {section_name}, When: {when}")
                when_jobs[section_name] = when

    return when_jobs


keyword_paragraphs = {
    "after_script": {
        "used": "Utilizing `after_script` in the `.gitlab-ci.yml` file ensures that specific commands run after the job's script phase has completed, regardless of the job's success or failure. This feature is beneficial for cleaning up resources, collecting logs, or performing any teardown tasks required for maintaining a clean and efficient CI/CD pipeline. Its inclusion helps ensure resources are managed efficiently and can aid in debugging by preserving logs or other data generated during the job.",
        "not_used": "The absence of `after_script` in your `.gitlab-ci.yml` file may indicate a missed opportunity for resource management and post-job cleanup. Incorporating `after_script` can significantly enhance the efficiency of your CI/CD pipeline by ensuring the execution of necessary cleanup or log collection tasks after each job. Consider using `after_script` to improve resource utilization and simplify debugging and audit trails.",
    },
    "allow_failure": {
        "used": "Implementing `allow_failure` within jobs signifies a strategic approach to CI/CD pipelines, allowing certain jobs to fail without impacting the overall pipeline success. This flexibility is crucial for experimental features, optional tests, or deployments where failure is an acceptable risk. The use of `allow_failure` ensures that the pipeline can continue to deliver value, even when specific, non-critical components fail, thereby maintaining momentum in development and testing cycles.",
        "not_used": "Not leveraging `allow_failure` could result in an overly stringent CI/CD pipeline where every job's success is critical, potentially halting progress for minor issues. Considering the use of `allow_failure` for non-essential jobs can prevent unnecessary blocks and keep the pipeline flowing smoothly, especially for experimental or less critical tasks. This approach balances rigor with flexibility, maintaining productivity without compromising quality.",
    },
    "artifacts": {
        "used": "The `artifacts` keyword is effectively used to specify files and directories created by a job that should be attached to the job upon completion. This is crucial for sharing outputs between jobs in a pipeline or preserving build outputs for post-pipeline access. By using `artifacts`, you ensure critical components like binaries, logs, or test reports are available where and when they are needed, facilitating a more interconnected and efficient CI/CD process.",
        "not_used": "If your `.gitlab-ci.yml` does not utilize `artifacts`, you might be missing out on the opportunity to efficiently share or archive job outputs. Artifacts are essential for passing compiled code, test results, or other outputs between stages or jobs, enhancing collaboration and efficiency. Consider defining artifacts to improve your pipeline's functionality and artifact management practices.",
    },
    "before_script": {
        "used": "The `before_script` keyword is employed to define commands that run before each job's script phase, setting up a consistent environment for job execution. This is instrumental in configuring prerequisites or ensuring the environment is correctly prepared, such as setting environment variables, installing dependencies, or performing preliminary checks. Including `before_script` enhances job reliability and consistency across your CI/CD pipeline.",
        "not_used": "Without `before_script`, each job may lack a standardized initial setup, potentially leading to inconsistent environments and results. Incorporating `before_script` can streamline your pipeline by ensuring each job starts with the necessary setup, reducing errors and inconsistencies. It's an opportunity to enforce a uniform starting point for all jobs, improving reliability and efficiency.",
    },
    "cache": {
        "used": "The `cache` keyword is adeptly used to specify files or directories that should be cached between jobs or pipeline runs, significantly reducing build time and bandwidth consumption. Caching dependencies or commonly reused files speeds up job execution by avoiding redundant downloads or computations. Its inclusion is a best practice for optimizing pipeline performance and efficiency.",
        "not_used": "Not utilizing `cache` in your pipeline configuration might lead to unnecessary repetition of tasks such as downloading dependencies, which can significantly increase build times and resource usage. Implementing caching strategies for dependencies or frequently accessed resources can greatly enhance the speed and efficiency of your CI/CD process. Consider adding `cache` to improve pipeline performance.",
    },
    "coverage": {
        "used": "The `coverage` keyword is wisely incorporated to extract code coverage information from job logs, providing a quantifiable measure of how much of your codebase is covered by tests. This is pivotal for maintaining high code quality and ensuring that critical paths are tested. By including `coverage`, you empower your team with actionable insights into the test suite's effectiveness, fostering a culture of quality and continuous improvement.",
        "not_used": "If your pipeline configuration overlooks the `coverage` keyword, you may be missing valuable insights into your test suite's coverage and effectiveness. Incorporating code coverage analysis into your CI/CD pipeline is essential for identifying untested code paths and continuously improving code quality. Consider leveraging `coverage` to gain a deeper understanding of your test suite's impact.",
    },
    "dast_configuration": {
        "used": "The integration of `dast_configuration` in your `.gitlab-ci.yml` file demonstrates a proactive stance on web application security by enabling Dynamic Application Security Testing (DAST). This approach allows for the automated detection of vulnerabilities in running web applications, ensuring continuous security assessment. Employing DAST as part of your CI/CD pipeline is crucial for early detection of security issues, helping to maintain the integrity and security of your applications.",
        "not_used": "The absence of `dast_configuration` suggests a potential gap in your security testing strategy. Incorporating Dynamic Application Security Testing (DAST) into your CI/CD pipeline can significantly enhance your ability to identify and mitigate vulnerabilities in web applications before they reach production. Consider adopting DAST to bolster your application security measures and protect against evolving security threats.",
    },
    "dependencies": {
        "used": "The `dependencies` keyword is strategically used to control the artifacts passed between jobs, allowing for fine-grained control over the flow of artifacts within the pipeline. This optimizes pipeline performance by ensuring that only necessary artifacts are transferred, reducing storage and bandwidth usage. Implementing `dependencies` supports efficient and effective artifact management, contributing to a streamlined and cost-effective CI/CD process.",
        "not_used": "Without specifying `dependencies`, your CI/CD pipeline might be transferring unnecessary artifacts between jobs, leading to wasted storage and increased job completion times. By defining explicit `dependencies`, you can ensure that only the required artifacts are passed along, optimizing resource usage and speeding up the pipeline. Consider utilizing `dependencies` for better artifact management.",
    },
    "default": {
        "used": "Employing the `default` keyword allows for the definition of global settings that apply to all jobs within the `.gitlab-ci.yml` file, ensuring consistency and reducing redundancy across your pipeline configuration. This includes settings for `image`, `services`, `before_script`, and more. Utilizing `default` enhances the maintainability of your CI/CD configuration and streamlines the setup process for multiple jobs.",
        "not_used": "Not leveraging the `default` keyword in your pipeline configuration may lead to unnecessary repetition and increased potential for inconsistency across jobs. By defining global defaults, you can ensure a consistent environment and settings for all jobs, making your pipeline configuration more efficient and easier to maintain. Consider using `default` to apply universal settings across your pipeline.",
    },
    "environment": {
        "used": "Utilizing the `environment` keyword allows for the specification of deployment environments in your `.gitlab-ci.yml` file, facilitating clear and manageable deployments to different stages such as production, staging, or testing. This is essential for implementing continuous deployment practices and ensures that your deployments are predictable and traceable. Including `environment` configurations enhances the automation of your deployment processes and supports a robust DevOps practice.",
        "not_used": "If `environment` configurations are not utilized in your pipeline, you might be missing out on streamlined deployment processes and the benefits of clear environment management. Defining environments in your CI/CD pipeline enables more controlled and automated deployments, improving both efficiency and reliability. Consider defining `environment` settings to better manage your deployment workflows.",
    },
    "extends": {
        "used": "The `extends` keyword is effectively used to reduce duplication in your `.gitlab-ci.yml` by allowing jobs to inherit configurations from templates or other jobs. This not only simplifies your CI/CD configuration but also ensures consistency across similar jobs. Employing `extends` contributes to a cleaner, more maintainable pipeline configuration, facilitating easier updates and modifications.",
        "not_used": "Not taking advantage of the `extends` keyword can lead to configuration duplication and a higher likelihood of inconsistencies across your pipeline. By using `extends` to inherit common configurations, you can streamline your CI/CD setup, reduce potential errors, and make your pipeline configuration easier to manage and understand. Consider using `extends` to improve the maintainability of your pipeline.",
    },
    "hooks": {
        "used": "Implementing `hooks` in your `.gitlab-ci.yml` allows for the execution of custom scripts or actions at specific points in the CI/CD process, such as before or after specific events. This enables fine-grained control over the pipeline's behavior, allowing for custom notifications, integrations, or other automated actions that enhance the pipeline's functionality and responsiveness. Including `hooks` can significantly increase the flexibility and extensibility of your CI/CD workflows.",
        "not_used": "The absence of `hooks` in your CI/CD configuration could indicate a missed opportunity to further automate and refine your pipeline processes. `Hooks` offer a powerful mechanism for integrating custom logic, notifications, or actions into your CI/CD workflows, enabling more dynamic and responsive pipelines. Consider incorporating `hooks` to take full advantage of GitLab CI/CD's capabilities.",
    },
    "id_tokens": {
        "used": "The use of `id_tokens` within your `.gitlab-ci.yml` facilitates secure, token-based authentication for jobs requiring access to external services or resources. This approach ensures that access credentials are managed securely and reduces the risk of exposing sensitive information. Leveraging `id_tokens` is a best practice for maintaining security and privacy within your CI/CD pipeline, particularly when interacting with external APIs or services.",
        "not_used": "If `id_tokens` are not utilized in your pipeline configuration, you may be missing a secure method for authenticating jobs to external services. Implementing `id_tokens` provides a secure, token-based authentication mechanism, enhancing the security of your pipeline and protecting access to external resources. Consider using `id_tokens` to ensure secure interactions with external services.",
    },
    "image": {
        "used": "The `image` keyword is crucial for defining the Docker image that will be used to run your CI/CD jobs, ensuring that the necessary environment, tools, and dependencies are available for job execution. This is essential for creating reproducible and consistent job environments, facilitating successful builds, tests, and deployments. Employing `image` contributes to the reliability and efficiency of your pipeline by standardizing the execution environment across jobs.",
        "not_used": "If your `.gitlab-ci.yml` does not specify `image`, you might be missing out on the benefits of a consistent and controlled execution environment for your CI/CD jobs. Utilizing Docker images allows for the creation of standardized environments that include all necessary dependencies, tools, and configurations, enhancing the reliability and reproducibility of your pipeline. Consider specifying `image` to standardize your job execution environments.",
    },
    "include": {
        "used": "Leveraging the `include` keyword enables the modularization of your `.gitlab-ci.yml` configuration by allowing the inclusion of external YAML files. This facilitates the reuse of common configurations, simplifies the management of large and complex pipelines, and promotes a cleaner, more organized CI/CD configuration. Including external configurations streamlines updates and ensures consistency across your pipeline.",
        "not_used": "Without utilizing `include`, your pipeline configuration may become cumbersome and difficult to manage, especially as it grows in complexity. By incorporating `include` to reference external YAML files, you can modularize and organize your CI/CD setup, making it easier to maintain and update. Consider using `include` to enhance the manageability and scalability of your pipeline configuration.",
    },
    "inherit": {
        "used": "Employing the `inherit` keyword allows for fine-tuned control over which configurations are inherited by jobs from the global context, ensuring that only necessary settings are applied. This can help prevent unintended side effects from global configurations and allow for more specific job configurations. Utilizing `inherit` enhances the precision and clarity of your pipeline configuration, contributing to more predictable and manageable CI/CD workflows.",
        "not_used": "Not taking advantage of `inherit` might lead to unnecessary complexity and potential configuration conflicts within your pipeline. By explicitly defining inheritance with the `inherit` keyword, you can ensure that jobs receive only the configurations they need, avoiding the application of unwanted global settings. Consider using `inherit` to streamline and clarify your pipeline configurations.",
    },
    "interruptible": {
        "used": "The `interruptible` keyword is wisely implemented to mark jobs as safe to cancel if newer pipelines are triggered, optimizing CI/CD resource utilization and efficiency. This is particularly useful for non-critical or long-running jobs that do not need to complete if superseded by more recent commits. Including `interruptible` in your pipeline configuration ensures more agile and resource-efficient CI/CD processes, reducing wait times and conserving resources.",
        "not_used": "If `interruptible` is not utilized in your CI/CD configuration, you may be unnecessarily consuming resources on jobs that could be safely interrupted by newer pipeline runs. This can lead to inefficiencies, such as longer wait times for critical jobs and wasted compute resources. Consider marking appropriate jobs as `interruptible` to enhance the agility and resource efficiency of your pipeline.",
    },
    "needs": {
        "used": "The `needs` keyword is strategically employed to define dependencies between jobs, allowing for the parallel execution of jobs that don't have direct dependencies. This optimization can significantly reduce pipeline execution time by enabling earlier stages to run concurrently with later ones, provided there are no interdependencies. The correct use of `needs` enhances pipeline efficiency by smartly managing job sequencing and dependencies.",
        "not_used": "Not taking advantage of the `needs` keyword in your `.gitlab-ci.yml` file might result in longer pipeline execution times due to a lack of parallelization. By defining job dependencies with `needs`, you can optimize your pipeline's execution time, allowing for faster feedback and more efficient resource use. Consider incorporating `needs` to improve the throughput of your CI/CD pipeline.",
    },
    "only": {
        "used": "The `only` keyword is effectively used to specify conditions under which a job should be included in a pipeline. This control mechanism allows for the dynamic inclusion of jobs based on branch names, tags, or other variables, ensuring that certain jobs run only when necessary. This targeted approach to job execution makes your pipeline more flexible and efficient, reducing runtimes and resource consumption for unnecessary jobs.",
        "not_used": "Without the use of `only`, your pipeline may be running jobs that are not always necessary, leading to wasted resources and longer execution times. Implementing `only` conditions can streamline your pipeline by ensuring jobs are only executed when their inclusion is relevant, such as for specific branches or in response to particular changes. Consider utilizing `only` to enhance the efficiency and relevance of your pipeline execution.",
    },
    "pages": {
        "used": "The `pages` keyword is adeptly utilized to automate the deployment of static websites or webpages directly from the GitLab CI/CD pipeline. This feature simplifies the process of updating and maintaining project documentation or websites, ensuring they are seamlessly updated with each pipeline run. Employing `pages` demonstrates a commitment to maintaining up-to-date project resources and facilitates easy access to documentation or project sites.",
        "not_used": "If your `.gitlab-ci.yml` file does not include the `pages` keyword, you might be missing out on an efficient way to manage and deploy your project's static websites or documentation. Utilizing `pages` can significantly streamline the deployment process, ensuring your project's website or documentation is automatically updated with each commit. Consider adopting `pages` for an effortless approach to website or documentation maintenance.",
    },
    "parallel": {
        "used": "The `parallel` keyword is judiciously used to indicate that a job should be split into multiple instances running in parallel, dramatically improving job execution times for tasks that can be parallelized. This is particularly useful for test suites or tasks that can be divided into independent units of work. By leveraging `parallel`, pipelines can achieve faster feedback cycles and more efficient use of resources.",
        "not_used": "The absence of `parallel` in job definitions may lead to missed opportunities for optimizing pipeline execution times. Tasks that are suitable for parallel execution, such as large test suites, can significantly benefit from being split into multiple concurrent instances. Incorporating parallel execution can.",
    },
    "release": {
        "used": "Utilizing the `release` keyword allows for the explicit definition and creation of releases directly within your `.gitlab-ci.yml`, linking the CI/CD pipeline with your project's release management. This facilitates a more integrated and automated approach to releasing, ensuring that every pipeline success can optionally result in a new release, complete with release notes and artifacts. Employing `release` showcases a sophisticated and automated approach to managing project releases, enhancing visibility and consistency.",
        "not_used": "Not utilizing the `release` keyword might indicate a missed opportunity for integrating release management with CI/CD processes. Automating release creation through the CI/CD pipeline can greatly enhance the consistency and efficiency of releasing new versions of your software, reducing manual overhead and potential errors. Consider using `release` to streamline your release process and better align it with your CI/CD workflows.",
    },
    "resource_group": {
        "used": "The `resource_group` keyword is cleverly used to limit concurrent execution of jobs that use shared resources, such as deployment environments or external test devices. This ensures that only one job within the group runs at a time, preventing conflicts and resource contention. The inclusion of `resource_group` in your CI/CD pipeline safeguards shared resources, ensuring orderly access and preventing disruptions in your deployment or testing processes.",
        "not_used": "If `resource_group` is not utilized, there may be a risk of jobs interfering with each other when accessing shared resources, leading to failures or unpredictable results. Implementing `resource_group` can mitigate this risk by serializing access to shared resources, ensuring jobs are executed in an orderly manner. Consider employing `resource_group` to enhance the stability and reliability of jobs that depend on limited shared resources.",
    },
    "retry": {
        "used": "The inclusion of `retry` in your `.gitlab-ci.yml` allows for automatic re-execution of jobs that fail under certain conditions, enhancing the resilience and reliability of your CI/CD pipeline. This is particularly useful in addressing transient issues such as network failures or temporary service outages, ensuring that such sporadic failures do not unnecessarily impede the progress of your pipeline. By judiciously applying `retry`, you maintain momentum in your development workflow and reduce manual intervention for sporadic failures.",
        "not_used": "Without utilizing the `retry` keyword, your pipeline may be more susceptible to failure due to transient issues, potentially leading to unnecessary manual interventions or delays in the workflow. Implementing a retry strategy for jobs prone to sporadic failures can significantly enhance the stability and efficiency of your CI/CD process. Consider defining `retry` parameters to minimize disruptions caused by transient issues.",
    },
    "rules": {
        "used": "Employing `rules` in your `.gitlab-ci.yml` enables fine-grained control over when jobs are executed, allowing for dynamic pipeline configurations based on various conditions such as changes in specific files, pipeline source, or manual triggers. This flexibility ensures that resources are allocated efficiently, by only running jobs when necessary, thereby optimizing pipeline performance and reducing waste. The strategic use of `rules` contributes to a smarter, conditionally adapted CI/CD process.",
        "not_used": "The absence of `rules` in your pipeline configuration could lead to inefficiencies, such as running jobs that are not necessary for certain changes or scenarios. By incorporating `rules`, you can make your pipeline smarter and more responsive to the specific context of each run, avoiding unnecessary executions and saving resources. Consider leveraging `rules` to dynamically adapt your pipeline according to relevant conditions.",
    },
    "secrets": {
        "used": "The proper use of `secrets` in your CI/CD configuration enhances security by safely managing sensitive data such as passwords, tokens, and keys. By securely injecting these secrets into your CI/CD jobs, you ensure that sensitive information is handled appropriately, maintaining the confidentiality and integrity of your systems and processes. Utilizing `secrets` effectively mitigates the risk of exposure and supports compliance with security best practices.",
        "not_used": "Not leveraging `secrets` for managing sensitive data in your CI/CD pipeline can pose significant security risks, including accidental exposure of sensitive information in logs or repository files. Implementing a secure method for handling `secrets` within your pipeline is critical for protecting sensitive data and maintaining a robust security posture. Consider integrating `secrets` management into your CI/CD practices to enhance security and compliance.",
    },
    "script": {
        "used": "The `script` keyword is crucial for defining the command or script that the job will execute. ...",
        "not_used": "Not utilizing the `script` keyword means your CI/CD jobs may not have specific commands or scripts to run, which is essential for the execution phase of your CI/CD pipeline. ...",
    },
    "services": {
        "used": "The use of `services` in your `.gitlab-ci.yml` file facilitates the integration of external services such as databases, caches, or any other supporting services that jobs might depend on during execution. This feature simplifies the setup of complex environments, allowing for more comprehensive and realistic testing scenarios. By defining `services`, you ensure that your jobs have access to the necessary external resources, closely mimicking production environments and enhancing the quality of testing and deployment processes.",
        "not_used": "If your CI/CD configuration does not include `services`, you may be missing out on the benefits of integrating external resources seamlessly into your pipeline. This can lead to challenges in replicating production-like environments for testing, potentially affecting the reliability of your deployment process. Incorporating `services` can significantly improve the fidelity of your testing environments and streamline the setup process for complex job dependencies.",
    },
    "spec": {
        "used": "The `spec` keyword, when used, specifies a detailed configuration for certain GitLab CI/CD features, such as Kubernetes deployments or other integrations. Including `spec` in your pipeline configuration allows for precise control over these integrations, ensuring that they are configured and behave exactly as needed for your development and deployment workflows. This precision is crucial for complex deployments that require detailed specifications to run correctly and efficiently.",
        "not_used": "Not making use of the `spec` keyword may indicate that your pipeline is not taking full advantage of GitLab's CI/CD capabilities for detailed specification of integrations and deployments. By defining specific configurations with `spec`, you can ensure greater accuracy and efficiency in your deployment processes, especially when dealing with complex integrations. Consider incorporating `spec` to enhance control over your CI/CD integrations and deployments.",
    },
    "stage": {
        "used": "Utilizing `stage` in your `.gitlab-ci.yml` file organizes jobs into distinct phases of the CI/CD pipeline, such as build, test, and deploy. This organization facilitates the sequential execution of jobs, ensuring that dependencies are respected and that resources are allocated efficiently. Proper use of `stage` enhances pipeline clarity and execution flow, making it easier to manage and understand the pipeline's progression.",
        "not_used": "Without clear `stage` definitions, your CI/CD pipeline might lack the necessary structure to ensure efficient and orderly execution of jobs. This can lead to resource contention, inefficient execution, and difficulties in pipeline management. Defining stages allows for a more organized and predictable pipeline execution, improving both efficiency and manageability. Consider employing `stage` to enhance the structure and flow of your CI/CD processes.",
    },
    "stages": {
        "used": "The `stages` keyword is crucial for defining the sequence and organization of stages within your CI/CD pipeline. By explicitly specifying stages, you ensure a logical progression of pipeline tasks, from initial code validation to deployment. This structured approach facilitates better resource management, clearer visibility into the pipeline's progress, and enhanced control over the build, test, and deploy phases. The use of `stages` is foundational to a well-organized and efficient pipeline.",
        "not_used": "Lacking a clear definition of `stages` in your `.gitlab-ci.yml` may result in a less organized pipeline, potentially leading to confusion and inefficiencies in the execution of CI/CD tasks. Defining stages provides a framework for the orderly progression of jobs, enhancing the pipeline's clarity and effectiveness. Consider defining `stages` to ensure a structured and efficient pipeline execution strategy.",
    },
    "tags": {
        "used": "The `tags` keyword allows for specifying particular runners that should execute the jobs, enabling precise control over the execution environment based on the requirements of each job. This can be crucial for jobs requiring specific hardware, software, or environments. By carefully selecting runners with `tags`, you ensure that each job runs in the most suitable context, optimizing performance and reducing execution times.",
        "not_used": "Not specifying `tags` for your jobs could result in them being picked up by any available runner, potentially leading to inconsistencies or suboptimal performance if the runner is not ideally suited for the job's requirements. Utilizing `tags` to direct jobs to the most appropriate runners can significantly enhance the efficiency and reliability of your CI/CD process. Consider defining `tags` to better match jobs with suitable execution environments.",
    },
    "timeout": {
        "used": "Implementing a `timeout` in your `.gitlab-ci.yml` configuration ensures that jobs are automatically terminated if they exceed a specified duration. This is a critical feature for managing resource allocation and avoiding prolonged or stuck jobs that can clog the pipeline. Setting a `timeout` promotes efficient pipeline flow and resource usage, ensuring that no single job can indefinitely occupy runners or delay the pipeline's progress.",
        "not_used": "Without specifying a `timeout` for your jobs, there's a risk that hung or excessively long-running jobs could monopolize resources and delay the pipeline. By setting a reasonable `timeout`, you can prevent such scenarios, ensuring a smoother and more predictable CI/CD process. Consider implementing `timeout` values to safeguard against resource hogging and pipeline delays.",
    },
    "trigger": {
        "used": "The `trigger` keyword is employed to define downstream pipeline triggers, allowing one project's pipeline to automatically initiate another project's pipeline. This facilitates complex workflows and integrations, enabling seamless automation across multiple projects. By leveraging `trigger`, you can create a cohesive CI/CD ecosystem that efficiently orchestrates the build, test, and deployment processes across interconnected projects.",
        "not_used": "If you're not using `trigger` in your CI/CD configuration, you may be missing out on the benefits of automated inter-project workflows. Without it, managing dependencies or coordinated deployments across projects can become manual and error-prone. Incorporating `trigger` can significantly enhance automation and efficiency, enabling more sophisticated and integrated CI/CD strategies.",
    },
    "variables": {
        "used": "Utilizing `variables` in your `.gitlab-ci.yml` file allows for the definition of environment variables accessible during job execution. This is essential for customizing job behavior, managing configurations, or storing non-sensitive data required during the pipeline's execution. By defining `variables`, you enhance the flexibility and adaptability of your CI/CD jobs, enabling them to be configured dynamically based on the pipeline's needs.",
        "not_used": "Not defining `variables` in your pipeline configuration could limit the flexibility and dynamic configuration capabilities of your CI/CD jobs. Variables play a crucial role in customizing and controlling the execution environment, making them indispensable for managing diverse or complex workflows. Consider leveraging `variables` to increase the configurability and efficiency of your CI/CD processes.",
    },
    "when": {
        "used": "The `when` keyword provides control over the execution conditions for jobs, allowing you to specify whether a job should run always, on failure, or on success. This conditional execution is crucial for optimizing resource usage and ensuring that subsequent jobs in the pipeline are triggered based on the desired outcomes of preceding jobs. By effectively using `when`, you can create a more intelligent and responsive CI/CD pipeline that adapts to the success or failure of individual jobs.",
        "not_used": "Without making use of the `when` keyword, your pipeline might be less efficient, executing jobs regardless of the context or outcomes of preceding jobs. This could lead to unnecessary resource consumption and potentially unhelpful job executions. Incorporating `when` allows for more nuanced control and optimization of your pipeline, ensuring jobs are executed only under the appropriate conditions.",
    },
    "workflow": {
        "used": "The `workflow` keyword is instrumental in controlling the overall pipeline execution rules, determining when and under what conditions a pipeline should run. This level of control is crucial for optimizing CI/CD operations, allowing you to prevent unnecessary runs in response to irrelevant changes or in specific branches. By employing the `workflow` keyword, you effectively manage pipeline triggers, ensuring that resources are allocated wisely and that pipelines run only when they provide real value to the development process.",
        "not_used": "The absence of the `workflow` keyword in your `.gitlab-ci.yml` configuration might lead to less efficient pipeline management, potentially resulting in pipelines running more frequently than necessary. This can consume valuable CI/CD resources and slow down the development process by occupying runners with unnecessary tasks. Incorporating `workflow` rules can significantly enhance pipeline efficiency by ensuring that pipelines are triggered only under conditions that are relevant to the project's needs, thereby optimizing resource use and focusing efforts where they are most beneficial.",
    },
}


def analyze_artifacts(parsed_yaml):
    jobs_with_artifacts = {"expiring": {}, "non_expiring": {}}

    for job_name, job_config in parsed_yaml.items():
        if isinstance(job_config, dict) and "artifacts" in job_config:
            artifacts_info = job_config["artifacts"]
            expire_in = artifacts_info.get("expire_in", None)
            artifact_paths = artifacts_info.get("paths", [])
            artifact_names = (
                ", ".join(artifact_paths)
                if artifact_paths
                else "No artifact paths defined"
            )

            artifact_entry = {
                "expire_in": expire_in or "never",
                "names": artifact_names,
            }

            if expire_in:
                jobs_with_artifacts["expiring"][job_name] = artifact_entry
            else:
                jobs_with_artifacts["non_expiring"][job_name] = artifact_entry

    return jobs_with_artifacts


def list_includes(parsed_yaml):
    # Function to find and list 'include' paths or files
    includes = parsed_yaml.get("include", [])
    include_paths = []

    if isinstance(includes, list):  # Handling list of includes
        for include in includes:
            if isinstance(include, dict):
                # Extract and format each key-value pair from the dictionary
                for key, value in include.items():
                    include_paths.append(f"<strong>{key}</strong>: {value}")
            else:
                # Assume string entries are URLs, defaulting the subkey to "url"
                include_paths.append(f"<strong>url</strong>: {include}")
    elif isinstance(includes, dict):  # Handling single include as a dict
        for key, value in includes.items():
            include_paths.append(f"<strong>{key}</strong>: {value}")
    else:
        # Handling single include as a string, default subkey as "url"
        include_paths.append(f"<strong>url</strong>: {includes}")

    print("\nIncluded paths/files (with associated subkeys):")
    for path in include_paths:
        print(f"- {path}")

    return include_paths


def output_results_html(
    used_keywords,
    unused_keywords,
    stages,
    job_count,
    stage_count,
    parsed_yaml,
    job_stages,
    stages_jobs_aggregated,
    jobs_with_artifacts,
    keyword_docs,
    jobs_with_rules,
    jobs_without_rules,
    jobs_with_only,
    jobs_with_except,
    jobs_with_needs,
    jobs_without_needs,
    include_paths,
    include_count,
):
    result_str = []
    result_str.append("<html><head><title>Pipeline Parser Results</title></head><body>")
    result_str.append(
        "<img src='https://images.ctfassets.net/xz1dnu24egyd/1hnQd13UBU7n5V0RsJcbP3/769692e40a6d528e334b84f079c1f577/gitlab-logo-100.png' alt='GitLab Logo' style='width: 695px; height: 152px;'>"
    )
    result_str.append("<h1>PipelineParser Results</h1>")
    result_str.append("<script>")
    result_str.append(
        """
        function toggleSection(sectionId) {
            var x = document.getElementById(sectionId);
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    """
    )
    result_str.append("</script>")
    result_str.append("<style>")
    result_str.append(
        """
        .collapsible {
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
            background-color: #e5e5e5;
        }

        .active, .collapsible:hover {
            background-color: #FCA326;
        }

        .content {
            display: none;
            padding: 0 18px;
            overflow: hidden;
            background-color: #E7E4F2;
        }
    """
    )
    result_str.append("</style>")

    # Key Observations Section
    key_observations = []

    # Overview of stage and job counts... for todal include jobs without stages
    key_observations.append(
        f"In total, there are <strong>{job_count} jobs spread across {stage_count} stages</strong> defined in the provided gitlab-ci.yml file."
    )
    # key_observations.append(f"In total, there are <strong>{(job_count+len(stages_jobs_aggregated['default']))} jobs spread across {stage_count} stages</strong> defined in the provided gitlab-ci.yml file.")

    if "default" in stages_jobs_aggregated and stages_jobs_aggregated["default"]:
        # There are jobs without assigned stages
        key_observations.append(
            "There are <strong>jobs without stages</strong> that are defaulting to the 'test' stage. Consider assigning these formally to 'test', or to a different stage if appropriate."
        )

    # Check for stages without jobs
    stages_without_jobs = [
        stage
        for stage in stages
        if stage not in stages_jobs_aggregated or not stages_jobs_aggregated[stage]
    ]
    if stages_without_jobs:
        key_observations.append(
            "There are <strong>stages without any jobs</strong>. Consider removing them."
        )

    # Check if there are artifacts that are not set to expired key observations
    if jobs_with_artifacts["non_expiring"]:
        key_observations.append(
            "There are jobs that generate <strong>artifacts that do not expire</strong>, which could lead to increased storage usage over time. Consider setting 'expire_in' for these artifacts to manage storage more effectively."
        )

    # Add observations for jobs without Rules
    proportion_without_rules = len(jobs_without_rules) / job_count if job_count else 0
    if proportion_without_rules > 0:
        key_observations.append(
            f'There are <strong>{len(jobs_without_rules)} jobs ({proportion_without_rules:.2%}) that do not include \'rules\'</strong>. Consider increasing the proportion of jobs using <a href="https://docs.gitlab.com/ee/ci/yaml/#rules">rules</a>, and moving away from the use of <a href="https://docs.gitlab.com/ee/ci/yaml/#only--except">only/except</a> conditions, which are being deprecated.'
        )

    # Add observations for jobs_with_only
    if jobs_with_only:
        key_observations.append(
            f"There are <strong>{len(jobs_with_only)} jobs using the <code>'only'</code> keyword</strong>, which is slated to be deprecated. Consider using rules instead."
        )

    # Add observations for jobs_with_except
    if jobs_with_except:
        key_observations.append(
            f"There are <strong>{len(jobs_with_except)} jobs using the <code>'except'</code> keyword</strong>, which is slated to be deprecated. Consider using rules instead."
        )

    # Add observations for jobs_without_needs
    proportion_without_needs = len(jobs_without_needs) / job_count if job_count else 0
    if proportion_without_needs > 0:
        key_observations.append(
            f"There are <strong>{len(jobs_without_needs)} jobs ({proportion_without_needs:.2%}) that do not include 'needs'</strong>. Consider increasing the proportion of jobs using <a href=\"https://docs.gitlab.com/ee/ci/yaml/#needs\">needs</a>."
        )

    # Add key observation about include count
    key_observations.append(f"You have <strong>{include_count} includes</strong>.")

    # Add Key Observations to the result_str
    result_str.append("<div style='background-color: #e5e5e5; padding: 10px;'>")
    result_str.append("<h2>Key Observations</h2>")
    if key_observations:
        result_str.append("<ul>")
        for observation in key_observations:
            result_str.append(f"<li>{observation}</li>")
        result_str.append("</ul>")
    else:
        result_str.append("<p>No key observations.</p>")
    result_str.append("</div>")

    # Stages & Jobs Section
    result_str.append("<h2>Stages & Jobs</h2>")
    result_str.append(
        "<button class='collapsible' onclick=\"toggleSection('stagesjobsSection')\">Show Jobs and their associated Stages</button>"
    )
    result_str.append("<div class='content' id='stagesjobsSection'><ol>")

    for stage in stages:  # Ensure you have a list of all stages here
        jobs = stages_jobs_aggregated.get(stage, [])
        if jobs:
            result_str.append(
                f"<li><strong>{stage}</strong> stage contains: {', '.join(jobs)}</li>"
            )
        else:
            # This stage has no jobs associated with it
            result_str.append(
                f"<li><strong>{stage}</strong> stage - THIS STAGE HAS NO JOBS</li>"
            )

    # Handle jobs without an assigned stage
    if "default" in stages_jobs_aggregated:
        unassigned_jobs = ", ".join(stages_jobs_aggregated["default"])
        result_str.append(
            f"<li><em><strong>no stage defined</strong></em> - THESE JOBS DO NOT HAVE AN ASSIGNED STAGE AND WILL DEFAULT TO USING THE <strong>'test'</strong> STAGE: {unassigned_jobs}</li>"
        )

    result_str.append("</ol></div>")

    # Used Keywords Section with detailed paragraphs
    result_str.append("<h2>Used Keywords</h2>")
    result_str.append(
        "<p>This section <strong>highlights functionality that you are currently taking advantage of</strong>, demonstrating a certain level of maturity and sophistication.</p>"
    )
    result_str.append(
        "<button class='collapsible' onclick=\"toggleSection('usedKeywordsSection')\">Toggle Used Keywords</button>"
    )
    result_str.append("<div class='content' id='usedKeywordsSection'><ul>")
    for keyword in used_keywords:
        result_str.append(
            f"<li>{keyword} - <a href='{keyword_docs[keyword]}'>Documentation</a></li>"
        )
        result_str.append(f"<p>{keyword_paragraphs[keyword]['used']}</p>")
    result_str.append("</ul></div>")

    # Unused Keywords Section with detailed paragraphs
    result_str.append("<h2>Unused Keywords</h2>")
    result_str.append(
        "<p>This section identifies keywords you are not leveraging and thus <strong>highlights opportunities to optimize your pipelines further</strong>.</p>"
    )
    result_str.append(
        "<button class='collapsible' onclick=\"toggleSection('unusedKeywordsSection')\">Toggle Unused Keywords</button>"
    )
    result_str.append("<div class='content' id='unusedKeywordsSection'><ul>")
    for keyword in unused_keywords:
        result_str.append(
            f"<li>{keyword} - <a href='https://docs.gitlab.com/ee/ci/yaml/#{keyword}'>Documentation</a></li>"
        )
        if (
            keyword in keyword_paragraphs
        ):  # Check if there's a paragraph for unused keywords
            result_str.append(f"<p>{keyword_paragraphs[keyword]['not_used']}</p>")
    result_str.append("</ul></div>")

    result_str.append("</body></html>")

    # Inside output_results_html function, after existing sections

    # Adding Artifact Analysis Section
    result_str.append("<h2>Artifact Analysis</h2>")
    result_str.append(
        "<p>This section <strong>identifies artifacts and whether or not they are configured to expire</strong>.</p>"
    )
    result_str.append(
        "<button class='collapsible' onclick=\"toggleSection('artifactSection')\">Show Artifacts Analysis</button>"
    )
    result_str.append("<div class='content' id='artifactSection'><ul>")

    # Expiring Artifacts
    result_str.append("<h3>Jobs with Expiring Artifacts</h3>")
    if jobs_with_artifacts["expiring"]:
        result_str.append("<ul>")
        for job, details in jobs_with_artifacts["expiring"].items():
            result_str.append(
                f"<li>{job} - produces {details['names']} - expires in {details['expire_in']}</li>"
            )
        result_str.append("</ul>")
    else:
        result_str.append("<p>No jobs with expiring artifacts found.</p>")

    # Non-Expiring Artifacts
    result_str.append("<h3>Jobs with Non-Expiring Artifacts</h3>")
    if jobs_with_artifacts["non_expiring"]:
        result_str.append("<ul>")
        for job, details in jobs_with_artifacts["non_expiring"].items():
            result_str.append(
                f"<li>{job} - produces {details['names']} - never expires, SHOULD THIS ARTIFACT EXPIRE?</li>"
            )
        result_str.append("</ul>")
    else:
        result_str.append("<p>No jobs with non-expiring artifacts found.</p>")

    result_str.append("</ul></div>")

    # Include a section related to "rules", 'only" and "except"
    result_str.append("<h2>Deprecation of 'only' and 'except' in favor of 'rules'</h2>")
    result_str.append(
        "<p>This section <strong>identifies the use of 'only/except' conditions, and evaluates the proportion of jobs using 'rules'</strong>.</p>"
    )
    result_str.append(
        "<button class='collapsible' onclick=\"toggleSection('rulesSection')\">Show the only, except & Rules section</button>"
    )
    result_str.append("<div class='content' id='rulesSection'><ul>")

    result_str.append(
        f'<p>The <a href="https://docs.gitlab.com/ee/ci/yaml/#only--except">only/except keywords</a> are being deprecated. Moreover, some believe that most, if not all, jobs should be covered by <a href="https://docs.gitlab.com/ee/ci/yaml/#rules">rules</a>. Currently, the proportion of jobs without \'rules\' is {proportion_without_rules:.2f} ({len(jobs_without_rules)} of {job_count}).</p>'
    )

    # Section for Jobs with "only"
    result_str.append(
        f"<h3>{len(jobs_with_only)} jobs with 'only' conditions (consider using rules instead)</h3>"
    )
    if jobs_with_only:
        result_str.append("<ul>")
        for job in jobs_with_only:
            result_str.append(f"<li>{job}</li>")
        result_str.append("</ul>")
    else:
        result_str.append("<p>No jobs found using the 'only' keyword. Great!</p>")

    # Section for Jobs with "except"
    result_str.append(
        f"<h3>{len(jobs_with_except)} jobs with 'except' conditions (consider using rules instead)</h3>"
    )
    if jobs_with_except:
        result_str.append("<ul>")
        for job in jobs_with_except:
            result_str.append(f"<li>{job}</li>")
        result_str.append("</ul>")
    else:
        result_str.append("<p>No jobs found using the 'except' keyword. Great!</p>")

    # Section for Jobs with "rules"
    result_str.append(
        f"<h3>{len(jobs_with_rules)} jobs with 'rules' (see how you are already using them)</h3>"
    )
    if jobs_with_rules:
        result_str.append("<ul>")
        for job in jobs_with_rules:
            result_str.append(f"<li>{job}</li>")
        result_str.append("</ul>")
    else:
        result_str.append(
            "<p>No jobs with 'rules' were found. Many, if not most or all, jobs should probably use rules.</p>"
        )

    # Section for Jobs without "rules"
    result_str.append(
        f"<h3>{len(jobs_without_rules)} jobs without 'rules' (look for opportunities to use them more)</h3>"
    )

    if jobs_without_rules:
        proportion_without_rules = len(jobs_without_rules) / job_count
        result_str.append("<ul>")
        for job in jobs_without_rules:
            result_str.append(f"<li>{job}</li>")
        result_str.append("</ul>")
    else:
        result_str.append("<p>All jobs include the 'rules' keyword. Good show!</p>")

    result_str.append("</ul></div>")

    # Section for Jobs with "needs"
    result_str.append("<h2>Evaluation of jobs with 'needs'</h2>")
    result_str.append(
        "<p>This section <strong>identifies the use of 'needs' conditions.</strong></p>"
    )
    result_str.append(
        "<button class='collapsible' onclick=\"toggleSection('needsSection')\">Show the jobs with needs section</button>"
    )
    result_str.append("<div class='content' id='needsSection'><ul>")

    result_str.append("Many pipelines will benefit from the use of 'needs'.")
    # Section for Jobs without "needs"
    result_str.append(
        f"<h3>{len(jobs_with_needs)} jobs with 'needs' (see how you are using them now)</h3>"
    )

    if jobs_with_needs:
        result_str.append("<ul>")
        for job in jobs_with_needs:
            result_str.append(f"<li>{job}</li>")
        result_str.append("</ul>")
    else:
        result_str.append(
            "<p>No jobs with 'needs' were found. Multiple jobs should probably use needs.</p>"
        )

    # Section for Jobs without "needs"
    result_str.append(
        f"<h3>{len(jobs_without_needs)} jobs without 'needs' (look for opportunities to use them more)</h3>"
    )

    if jobs_without_needs:
        proportion_without_needs = len(jobs_without_needs) / job_count
        result_str.append("<ul>")
        for job in jobs_without_needs:
            result_str.append(f"<li>{job}</li>")
        result_str.append("</ul>")
    else:
        result_str.append("<p>All jobs include the 'needs' keyword.</p>")

    # # Section for Jobs with "needs"
    # result_str.append(f"Many pipelines will benefit from the user of 'needs'. There are currently {len(jobs_with_needs)} jobs using 'needs' conditions")
    # proportion_with_needs = (len(jobs_with_needs) / job_count)

    # if jobs_with_needs:
    #     result_str.append("<ul>")
    #     for job in jobs_with_needs:
    #         result_str.append(f"<li>{job}</li>")
    #     result_str.append("</ul>")
    # else:
    #     result_str.append("<p>No jobs found using the 'needs' keyword. This seems unusual.</p>")

    result_str.append("</ul></div>")

    # Include a section for 'includes'
    result_str.append("<h2>Included Subkeys:Paths/Files</h2>")
    result_str.append(
        "<p>This section lists the files or paths that are included in the GitLab CI configuration file.</p>"
    )
    result_str.append(
        "<button class='collapsible' onclick=\"toggleSection('includesSection')\">Toggle Included Files/Paths</button>"
    )
    result_str.append("<div class='content' id='includesSection'><ul>")
    if include_paths:
        for path in include_paths:
            result_str.append(f"<li>{path}</li>")
    else:
        result_str.append("<p>No includes found.</p>")
    result_str.append("</ul></div>")

    # Final output output
    # Include a div with a height of 150px to ensure that there is some room at the end.
    result_str.append("<div style='height: 150px'></div>")

    result_str = "\n".join(result_str)

    # Output to console for debugging (or remove in production code)
    print(result_str)

    # Output results to an HTML file
    with open("results.html", "w") as file:
        file.write(result_str)

    print("Results saved as results.html.")
    sys.exit(0)


def main():
    # Main function to execute the script
    if len(sys.argv) != 2:
        print("Usage: python analyze_gitlab_ci.py <gitlab-ci-file>")
        sys.exit(1)

    file_path = sys.argv[1]

    try:
        parsed_yaml = load_yaml(file_path)
    except Exception as e:
        print(f"Error loading YAML file: {e}")
        sys.exit(1)

    # Perform necessary analyses
    stages = list_stages(parsed_yaml)
    include_paths = list_includes(parsed_yaml)  # List included files or paths

    used_keywords, keyword_docs = check_gitlab_keywords(
        parsed_yaml
    )  # Fetch keyword_docs here
    unused_keywords = check_gitlab_unused_keywords(parsed_yaml)

    # Count the number of stages
    stage_count = count_stages(parsed_yaml)

    # List jobs and their stages
    job_stages = list_jobs_stages(parsed_yaml)

    # List stages and their jobs
    (
        stages_jobs_aggregated,
        jobs_with_rules,
        jobs_without_rules,
        jobs_with_only,
        jobs_with_except,
        jobs_with_needs,
        jobs_without_needs,
    ) = list_stages_jobs_aggregated(parsed_yaml)

    # Calculate the number of jobs
    job_count = len(job_stages)

    # Analyze Artifacts
    jobs_with_artifacts = analyze_artifacts(parsed_yaml)

    # Analyze Includes
    include_paths = list_includes(parsed_yaml)  # Capture include paths
    include_count = len(include_paths)  # Count the number of includes

    # Output results to a html file
    output_results_html(
        used_keywords,
        unused_keywords,
        stages,
        job_count,
        stage_count,
        parsed_yaml,
        job_stages,
        stages_jobs_aggregated,
        jobs_with_artifacts,
        keyword_docs,
        jobs_with_rules,
        jobs_without_rules,
        jobs_with_only,
        jobs_with_except,
        jobs_with_needs,
        jobs_without_needs,
        include_paths,
        include_count,
    )


if __name__ == "__main__":
    main()
