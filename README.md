# GitLab CI Pipeline Parser

This Python script analyzes a GitLab CI/CD pipeline configuration file (` .gitlab-ci.yml`) and generates an HTML report (results.html) with various insights about the pipeline. The report includes information about the stages, jobs, used and unused keywords, artifacts, and more.

## Features

- Count the total number of stages and jobs
- List all stages and the jobs within each stage
- Identify jobs without assigned stages
- Highlights used and unused GitLab CI/CD keywords
- Analyze job artifacts and their expiration settings
- Evaluate the use of rules, only, except, and needs keywords
- List included paths or files

## Prerequisites

- Python 3.x
- PyYAML library (`pyyaml`)

## Installation

1. Clone the repository:

   ```
   # git clone https://gitlab.com/ci-product-coach-pipeline-parser/pipeline-parser-2-0.git
   ```

2. Navigate to the cloned directory:

   ```
   # cd <repository_directory>
   ```

## Usage

- Ensure your `.gitlab-ci.yml` file is located in the same directory as the script, or provide the path to your `.gitlab-ci.yml` file.

- Run the script:

  ```
  # python parse_pipeline_script.py <path_to_gitlab_ci_yml>
  ```

  Replace `<path_to_gitlab_ci_yml>` with the actual path to your `.gitlab-ci.yml` file. If the file is in the same directory as the script, you can simply use:

  ```
  # python parse_pipeline_script.py .gitlab-ci.yml
  ```

- Check the output: The script will generate an HTML file named `results.html` in the same directory. Open this file in a web browser to view the analysis results.

## For GitLab CI Product Coach Consultations

- Provide a copy of the generated results.html with the Customer Success Engineer who will be hosting your consultation.

## Contributing

### Development

In order to contribute, please follow the following instructions to work on the code:

- Use a local Python environment (ie. virtualenv)
- Install and activate pre-commit
  - `# pip install pre-commit`
  - `# pre-commit install`
- `pre-commit` should now take care of reformatting your code for best Python coding practice as well as well as spot errors early

### Maintainers

In order to benefit from latest developments and updates, please run

`# pre-commit autoupdate`

on a regular basis.
